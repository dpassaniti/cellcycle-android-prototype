#ifndef CUSTOM_GLYPHS_H
#define CUSTOM_GLYPHS_H

#include <string>

char STEM_GLYPH = 0xC0;
char HEALTHY_GLYPH = 0xC1;
char MUTATED_GLYPH = 0xC2;
char CANCER_GLYPH = 0xC3;
char ARRESTED_GLYPH = 0xC4;
char BV_GLYPH = 0xC5;
char SPLIT_MODE_GLYPH = 0xC6;

#endif