#include "GlobalScoreValues.h"

int T3E::SCORE::spawned_healthy_cell_;
int T3E::SCORE::spawned_mutated_cell_;
int T3E::SCORE::spawned_cancer_cell_;
int T3E::SCORE::spawned_bloodvessel_;
int T3E::SCORE::spawned_stem_cell_;
int T3E::SCORE::arrested_cell_;
		
int T3E::SCORE::killed_healthy_cell_;
int T3E::SCORE::killed_mutated_cell_;
int T3E::SCORE::killed_cancer_cell_;
int T3E::SCORE::killed_bloodvessel_;
int T3E::SCORE::killed_stem_cell_;
int T3E::SCORE::killed_arrested_cell_;

int T3E::SCORE::cancer_per_second_;