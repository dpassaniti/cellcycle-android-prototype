#ifndef COMMANDS_H
#define COMMANDS_H

//game flow control
enum class command {NONE, PLAY, TUTORIAL, CREDITS, MENU, QUIT};

#endif